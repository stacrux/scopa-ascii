//Scopa V 1.00 20/12/10 
//Scopa V 1.11 7/01/11
#include<stdio.h>
#include<windows.h>
#include<string.h>


char* mazzo[40] = {"AC","2C","3C","4C","5C","6C","7C","JC","QC","KC",
                "AQ","2Q","3Q","4Q","5Q","6Q","7Q","JQ","QQ","KQ",
                "AF","2F","3F","4F","5F","6F","7F","JF","QF","KF",
                "AP","2P","3P","4P","5P","6P","7P","JP","QP","KP"};
char* easy[40] = {"AC","2C","3C","4C","5C","6C","7C","JC","QC","KC",
                "AQ","2Q","3Q","4Q","5Q","6Q","7Q","JQ","QQ","KQ",
                "AF","2F","3F","4F","5F","6F","7F","JF","QF","KF",
                "AP","2P","3P","4P","5P","6P","7P","JP","QP","KP"};                            
char* table[8];
char* mano[3];
char* pchand[3]; 
int pcpoints = 0;
int points = 0;
char* puntipc[40];
char* punti[40];
int q = 0;
int debug = 0;
int napula[10];
int quadri = 0;
int carte = 0;
int pcprimiera = 0;
int primi = 0;
char* primiera[4];
int ini = 1;
int scope = 0,pcscope = 0;
int e,i,a,b,m = 0,c = 0,somma = 0,somma1 = 0,sel = 0,turno = 0,apt = 0,n = 0,z = 1,k,l;
int ch1 = 0,ch2 = 1,ch3 = 0;
int card = 0;
char tav[13][50];
char* chcard[8];
char cartach[2];
char* sommamnt[8];
typedef struct{
        int x;
        int y;
        char g;}pointer;
char usr[30];        
pointer arr;

void distav();
void pesca();
void pcmossa();
void mossa();
void disegna();
int partita();
int check();
int pcheck();
int best(int num);
int ctoi(char cd);
void reset();
void contapunti();
char seme(char sm);
int prim(char pm);

int main(){
    system("color F0");
        
    srand(time(NULL));
    
    printf("Stai per giocare una partita a scopa contro il pc, il gioco verra' eseguito\n");
    printf("finche' un giocatore non supera 11 punti.\n");
    printf("In questo gioco e' applicata la variante 'asso piglia tutto' a meno che non sia gia' presente un asso in tavola.\n");
    printf("il computer stabilira' in maniera casuale chi iniziera' la partita\n");
    printf("usare i tasti direzionali e invio per selezionare una o piu' carte, per buttare una carta selezionare un'area vuota\n");
    printf("buona fortuna...\n");
    printf("nota : se ti trovi bloccato o vuoi deselezionare una carta premi esc\n");
    printf("\n\npremere invio per continuare");
    
   while(GetKeyState(13) >= 0) 
    sleep(1);
            
    system("cls");
    
    printf("inserire nome giocatore : ");
    scanf("%s",usr);
      
    
    ini = rand()%2;
    if(ini == 0)
     { printf("\ninizia il pc");
       turno = 1;
       sleep(2000);}
     if(ini == 1)
     { printf("\ninizi tu");
       sleep(2000);}  
       
        
    system("cls");
   for(e=0;e<3;e++)
     mano[e] = (char*)malloc(sizeof(char)*2);
   for(e=0;e<3;e++)
     pchand[e] = (char*)malloc(sizeof(char)*2);
   for(e=0;e<30;e++)
     table[e] = (char*)malloc(sizeof(char)*2);
   for(e=0;e<8;e++)
     chcard[e] = (char*)malloc(sizeof(char)*2); 
   for(e=0;e<4;e++)
     primiera[e] = (char*)malloc(sizeof(char)*2);    
  
   for(e=0;e<8;e++)
     sommamnt[e] = (char*)malloc(sizeof(char)*2); 
   for(e=0;e<40;e++)
     puntipc[e] = (char*)malloc(sizeof(char)*2); 
   for(e=0;e<40;e++)
     punti[e] = (char*)malloc(sizeof(char)*2);    
  printf("carte in tavola : \n"); 
   
   for(e=0;e<10;e++)
      napula[e] = 0; 
   for(e=0;e<8;e++)
             strcpy(chcard[e],"00");
   for(e=0;e<4;e++)
             strcpy(primiera[e],"00");
   for(e=0;e<8;e++)
             strcpy(sommamnt[e],"00");
   for(e=0;e<40;e++)
             strcpy(punti[e],"00");
   for(e=0;e<40;e++)
             strcpy(puntipc[e],"00");          
   
   
   for(i=0;i<4;i++){ 
      e = rand()%100;
     if(e < 40 && strcmp(mazzo[e],"00") != 0){
          strcpy(table[i],mazzo[e]);
          //printf("%s\n",table[i]);
          mazzo[e] = malloc(sizeof(char)*2);
          strcpy(mazzo[e],"00");
          }
     else i--;}                 
   for(i=i;i<8;i++)
    strcpy(table[i],"00");
   //  printf("%s\n",table[i]);
       pesca();
   
   
 distav();
   
            arr.x = 2;         
            arr.y = 29;
            arr.g = 25;
   
     disegna();
  
   while(1){    
     while(partita()){
            if(ini == 1){
             mossa();
             pcmossa();}
            else if(ini == 0){
             pcmossa(); 
             mossa();}
            if(ini == 0 && strcmp(mano[0],"00") == 0 && strcmp(mano[1],"00") == 0 && strcmp(mano[2],"00") == 0)
             { printf("\nora visto che non avete piu' carte in mano le distribuisco\n",pchand[0],pchand[1],pchand[2]);
               sleep(1000);
               turno = 1;
               pesca();
               disegna();
               }
            if(ini == 1 && strcmp(pchand[0],"00") == 0 && strcmp(pchand[1],"00") == 0 && strcmp(pchand[2],"00") == 0)
             { printf("\nora visto che non avete piu' carte in mano le distribuisco\n",pchand[0],pchand[1],pchand[2]);
               sleep(1000);
               turno = 0;
               pesca(); 
               disegna();
               }
               
             if(ini == 0 && strcmp(mano[0],"00") == 0 && strcmp(mano[1],"00") == 0 && strcmp(mano[2],"00") == 0)
               {  printf("\nora sistemo le carte in tavola che vanno all'ultimo che ha preso\n");
                  sleep(2000);
                     if(q == 0){
                                        for(e=0;e<40;e++) 
                                          for(i=0;i<8;i++) 
                                            if(*punti[e] == '0' && *table[i] != '0') 
                                                    { strcpy(punti[e],table[i]); 
                                                       strcpy(table[i],"00"); } 
                                      disegna();
                                      printf("\ntu hai preso per ultimo, e' tutto tuo\n");
                                      contapunti();
                                      //sleep(10000);
                                      }
                  else if(q == 1){
                                        for(e=0;e<40;e++) 
                                          for(i=0;i<8;i++) 
                                            if(*puntipc[e] == '0' && *table[i] != '0') 
                                                    { strcpy(puntipc[e],table[i]); 
                                                       strcpy(table[i],"00"); } 
                                            disegna();
                                      printf("\nil pc ha preso per ultimo, e' tutto suo\n");
                                      contapunti();
                                      //sleep(10000);
                                      }           
                   }  
             
             
             
             
             if(ini == 1 && strcmp(pchand[0],"00") == 0 && strcmp(pchand[1],"00") == 0 && strcmp(pchand[2],"00") == 0)
               {  printf("\nora sistemo le carte in tavola che vanno all'ultimo che ha preso\n");
                  sleep(2000);
                     if(q == 0){
                                        for(e=0;e<40;e++) 
                                          for(i=0;i<8;i++) 
                                            if(*punti[e] == '0' && *table[i] != '0') 
                                                    { strcpy(punti[e],table[i]); 
                                                       strcpy(table[i],"00"); } 
                                      disegna();
                                      printf("\ntu hai preso per ultimo, e' tutto tuo\n");
                                      contapunti();
                                      }
                  else if(q == 1){
                                        for(e=0;e<40;e++) 
                                          for(i=0;i<8;i++) 
                                            if(*puntipc[e] == '0' && *table[i] != '0') 
                                                    { strcpy(puntipc[e],table[i]); 
                                                       strcpy(table[i],"00"); } 
                                            disegna();
                                      printf("\nil pc ha preso per ultimo, e' tutto suo\n");
                                      contapunti();
                                      }           
                   }
             
             }
           
           if(pcpoints < 10 && points < 10)
           reset();  
           if(pcpoints > 10 || points > 10)
           break;  
           }//while(1)    
             
    if(pcpoints > points){
    system("cls");            
    printf("\n\n\n\nmi spiace non sei riuscito a battere il mio pc....su dai non piangere HAHAHAHA\n\n");}
    else{
    system("cls");     
    printf("\n\n\n\ncongratulazioni %s hai vinto questa partita !!!!!!!\n\n",usr);
    }
    
    printf("Ringraziamenti :\nringrazio davide per aver aiutato come beta tester e aver scovato tutti i metodi con cui il\n");
    printf("pc voleva imbrogliarlo XD, ringrazio colo che ha definito carino l'impianto grafico, anche se in realta' fa schifo\n");
    system("PAUSE");
}




//Definisco le funzioni

int partita(){ 
    i = 0; 
     for(e=0;e<40;e++)
      if(strcmp(mazzo[e],"00") != 0)
       i = 1;
    if(i == 0)
      {  for(e=0;e<3;e++)
          if(*mano[e] != '0' || *pchand[e] != '0')
            i = 1; }
    return i;}  

void pesca(){
    // printf("la tua mano\n");
    a=0;
     for(n=0;n<40;n++)
      if(*mazzo[n] != '0')
       break;
   
   if(n == 40){
    printf("\nbeh pare che il mazzo sia finito\n");
    sleep(1000);}
     
   if(n != 40){
    for(k=0;k<40;k++)
     if(*mazzo[k] != '0')
      a++;      
    printf("\nnel mazzo ci sono ancora %d carte\n",a);  
    printf("\nsono alla ricerca di carte disponibili\n");      
     for(i=0;i<3;i++){ 
      e = rand()%100;
     if(e < 40 && strcmp(mazzo[e],"00") != 0){
          strcpy(mano[i],mazzo[e]);
          //printf("%s\n",mano[i]);
          printf("carta per il giocatore ... fatto\n");
          mazzo[e] = malloc(sizeof(char)*2);
          strcpy(mazzo[e],"00");
          }
     else i--;
     sleep(35);}
    // printf("la mano del pc\n");
     for(i=0;i<3;i++){ 
      e = rand()%100;
     if(e < 40 && strcmp(mazzo[e],"00") != 0){
          strcpy(pchand[i],mazzo[e]);
          //printf("%s\n",pchand[i]);
          printf("carta per il pc        ... fatto\n");
          mazzo[e] = malloc(sizeof(char)*2);
          strcpy(mazzo[e],"00");
          }
     else i--;
     sleep(39);}} 
     
     sleep(1000);
     }    


int check(){
    i = 1;
    apt = 0;
    somma = 0;
    for(a=0;a < 8;a++)
     if(*table[a] == 'A')
      i=0;
     
    if(i==1){ 
         i=0;     
        for(a=0;a < 3;a++)
         if(*mano[a] == 'A'){
          i++;
           } }
      printf("\nhai a disposizione %d assi per prendere tutto\n",i);
      apt = i;
      
    for(e=0;e < 8;e++){
                    for(a=0;a<3;a++){
                                                   if(*mano[a] == *table[e] && *mano[a] != '0'){
                                                    i++; 
                                                    printf("\npuoi prendere il %c\n",*table[e]); }  } }

  for(e=0;e<8;e++){
    if(strcmp(table[e],"00") != 0){               
     somma = ctoi(*table[e]);
     somma1 = ctoi(*table[e]);
     for(a=0;a < 8;a++){
         //somma semplice 
          if( a > e && strcmp(table[a],"00") != 0){  
            somma = somma + ctoi(*table[a]);
             for(b=0;b<3;b++)
               if(ctoi(*mano[b]) == somma)
                 { i++;
                 //  printf("\nsomma: heila pare ci sia una somma (%c + %c) con %c\n",*table[e],*table[a],*mano[b]); 
                 }
             somma = somma - ctoi(*table[a]);
              // printf("\n ho appena controllato somma, passo a somma1 che vale %d\n",somma1);
                       }
          //somma semplice
          if( a != e && strcmp(table[a],"00") != 0){              
            if(somma1 + ctoi(*table[a]) < 11){
            //  printf("\nvisto che %d + %c < 11 somma1 diventa %d\n",somma1,*table[a],somma1 + ctoi(*table[a]));        
              somma1 = somma1 + ctoi(*table[a]);
             // printf("\nma porca troia ora somma1 vale %d\n",somma1);
              for(b=0;b<3;b++){
              // printf("\nsomma1 che vale %d\n",somma1);
               if(somma1 == ctoi(*mano[b]) && somma1 - ctoi(*table[a]) != somma )
                 { i++;
//printf("\nsomma1 :heila pare ci sia una somma (%d + %c) che fa %d con %c\n",somma1-ctoi(*table[a]),*table[a],somma1,*mano[b]); 
}                    
                   }
               }
            // else printf("\ninvece visto che %d + %c > 10 somma1 resta %d\n",somma1,*table[a],somma1);  
              }
       } } }      
       return i;  }
///////////////////////////////////////////////////
int pcheck(){
    i = 1;
    somma = 0;
    for(a=0;a < 8;a++)
     if(*table[a] == 'A')
      i=0;
     
    if(i==1){ 
         i=0;     
        for(a=0;a < 3;a++)
         if(*pchand[a] == 'A'){
          i++;
           } }
      
      apt = i;
      
    for(e=0;e < 8;e++){
                    for(a=0;a<3;a++){
                                                   if(*pchand[a] == *table[e] && *pchand[a] != '0'){
                                                    i++; 
                                                    //printf("\n'esso' pu� prendere il %c con la carta al posto numero %d\n",*table[e],a);
                                                    
                                                                          }  } }

  for(e=0;e<8;e++){
    if(strcmp(table[e],"00") != 0){               
     somma = ctoi(*table[e]);
     somma1 = ctoi(*table[e]);
     for(a=0;a < 8;a++){
         //somma semplice 
          if( a > e && strcmp(table[a],"00") != 0){  
            somma = somma + ctoi(*table[a]);
             for(b=0;b<3;b++)
               if(ctoi(*pchand[b]) == somma)
                 { i++;
                   //printf("\nsomma: heila pare ci sia una somma (%c + %c) con %c\n",*table[e],*table[a],*mano[b]); 
                 }
             somma = somma - ctoi(*table[a]);
               //printf("\n ho appena controllato somma, passo a somma1 che vale %d\n",somma1);
                       }
          //somma semplice
          if( a != e && strcmp(table[a],"00") != 0){              
            if(somma1 + ctoi(*table[a]) < 11){
              //printf("\nvisto che %d + %c < 11 somma1 diventa %d\n",somma1,*table[a],somma1 + ctoi(*table[a]));        
              somma1 = somma1 + ctoi(*table[a]);
              //printf("\nma porca troia ora somma1 vale %d\n",somma1);
              for(b=0;b<3;b++){
              // printf("\nsomma1 che vale %d\n",somma1);
               if(somma1 == ctoi(*pchand[b]) && somma1 - ctoi(*table[a]) != somma )
                 { i++;
//printf("\nsomma1 :heila pare ci sia una somma (%d + %c) che fa %d con %c\n",somma1-ctoi(*table[a]),*table[a],somma1,*mano[b]); 
}                    
                   }
               }
             //else printf("\ninvece visto che %d + %c > 10 somma1 resta %d\n",somma1,*table[a],somma1);
            // getch();  
              }
       } } }      
       return i;  }
//////////////////////////////////////////////////////////////////////////



void mossa(){
   
    
  if(turno == 0){   
   if(card == 0){
          sel = 0;  
          if(GetKeyState(VK_LEFT) < 0 && arr.y != 29)
           { arr.y = arr.y - 6;
             disegna(); }   
          if(GetKeyState(VK_RIGHT) < 0 && arr.y != 41)
           { arr.y = arr.y + 6;  
            disegna(); }
          if(GetKeyState(13) < 0 && *mano[(arr.y-29)/6] != '0'){
                            if(check() != 0){ 
                              card = ctoi(tav[5][arr.y]);
                              strcpy(cartach,mano[(arr.y - 29)/6]);
                              arr.x = 5;
                              n = arr.y;
                              arr.y = 3;
                              arr.g = 24;
                              disegna();
                              sleep(200);
                              printf("\ncarta selezionata : %c%c\n",*cartach,seme(*(cartach + 1)));}
                            else{
                                for(e=0;e<8;e++){
                                                 if(strcmp(table[e],"00") == 0)
                                                   { strcpy(table[e],mano[(arr.y - 29)/6]);
                                                     break;} }
                                strcpy(mano[(arr.y - 29)/6],"00");
                                disegna();
                                turno = 1;
                                }
                            if(*cartach == 'A' && apt > 0){
                                        
                                        for(e=0;e<40;e++){ 
                                       //  printf("\nsono nella for da 40 al n %d\n",e);                  
                                          for(k=0;k<8;k++){ 
                                                            
                                          //  printf("\npunti vele %s ,table vale %s\n",punti[e],table[k]);
                                            //sleep(2000);                
                                            if(*punti[e] == '0' && *table[k] != '0') 
                                                    {  strcpy(punti[e],table[k]); 
                                                       printf("\nmetto la carta %c%c nei tuoi punti\n",*punti[e],seme(*(punti[e]+1)));
                                                       sleep(500);
                                                       strcpy(table[k],"00"); } } }
           for(e=0;e<40;e++)
             if(*punti[e] == '0')
              { strcpy(punti[e],cartach);
                break; }
                                       
               strcpy(mano[(n - 29)/6],"00"); 
                                        
            
            
                      disegna();
                      printf("\nhai preso tutto con l'asso\n");                   
                      q = 0;
                      turno = 1;
                      }
                                        
                                        
                             //piglia tutto 
                                 } 
                               
            }
    if(card != 0 && turno == 0){
          if(GetKeyState(VK_ESCAPE) < 0)
           { card  = 0;
             sel = 25;
             turno = 0;}  
          if(GetKeyState(VK_LEFT) < 0 && arr.y != 3)
           { arr.y = arr.y - 6;
             disegna();}   
          if(GetKeyState(VK_RIGHT) < 0 && arr.y != 21)
           { arr.y = arr.y + 6;  
            disegna(); }        
          if(GetKeyState(VK_UP) < 0 && arr.x != 5)
           { arr.x = arr.x - 2;
             arr.g = 24;  
            disegna(); } 
          if(GetKeyState(VK_DOWN) < 0 && arr.x != 7)
           { arr.x = arr.x + 2;
             arr.g = 25;   
            disegna(); }
          if(GetKeyState(13) < 0){
                              if(arr.x == 7 && *table[((arr.y + 3)/6)+3]== '0' && sel == 0){
                               strcpy(table[((arr.y + 3)/6)+3],cartach);
                               for(n=0;n<3;n++)
                                if(strcmp(mano[n],cartach) == 0)
                                 strcpy(mano[n],"00");
                                  strcpy(cartach,"00");
                               disegna();
                               turno = 1;}
                               if(arr.x == 5 && *table[(arr.y -3)/6]== '0' && sel == 0 ){
                               strcpy(table[(arr.y - 3)/6],cartach);
                               for(n=0;n<3;n++)
                                if(strcmp(mano[n],cartach) == 0)
                                 strcpy(mano[n],"00");
                                  strcpy(cartach,"00");
                               disegna();
                               turno = 1;}
                              }
                      // qui sotto nella parentesi     && arr.x == 5 && tav[3][arr.y - 1] != '0'     
     if(GetKeyState(13) < 0 && turno == 0)
           { 
                             
             for(e=0;e<8;e++){ //questo for � necessario per non scegliere pi� volte la stessa carta nelle somme manuali
              if(arr.x == 5 && strcmp(chcard[e],table[(arr.y - 3)/6]) == 0)
              break;
              if(arr.x == 7 && strcmp(chcard[e],table[((arr.y + 3)/6)+3]) == 0)
              break;
              }
            if(e==8){  
             if(arr.x == 5)          
               sel = sel + ctoi(tav[3][arr.y - 1]);
             if(arr.x == 7)
               sel = sel + ctoi(tav[9][arr.y]);
             if(arr.x == 5)
               strcpy(chcard[b],table[(arr.y - 3)/6]); 
             else if(arr.x == 7)
               strcpy(chcard[b],table[((arr.y + 3)/6)+3]);
             disegna();
             //printf("\n%s\n",chcard[b]);
             b++;}
             sleep(200); }  
          
          for(e=0;e<8;e++)
           if(*table[e] == *cartach)
            if(sel != ctoi(*cartach) && sel > 0)
             { sel = 25;
               printf("\nnon fare il furbo devi per forza prendere il %c in tavola\n",*cartach);
               sleep(2000);
                break;}
                
                
          if(sel == card){
            printf("\nok adesso vedo...\n");
            
              for(e=0;e<8;e++)
               for(i=0;i<8;i++)
                if(strcmp(chcard[i],table[e]) == 0)
                 strcpy(table[e],"00");
                  
            for(e=0;e<40;e++) 
              for(i=0;i<8;i++) 
                if(*punti[e] == '0' && *chcard[i] != '0') 
                 { strcpy(punti[e],chcard[i]); 
                   strcpy(chcard[i],"00"); }   
              
            for(e=0;e<40;e++)
             if(*punti[e] == '0')
              { strcpy(punti[e],cartach);
                break; }
                 
              for(e=0;e<3;e++)
               if(strcmp(mano[e],cartach) == 0) 
                 strcpy(mano[e],"00");
                 
              for(n=0;n<8;n++)
               if(*table[n] != '0')
                break;
              for(l=0;l<40;l++)
               if(*mazzo[l] != '0')
                break;
              
              if(n==8 && *cartach != 'A'){
                if(l != 40){
                 printf("\nma questa e' una scopa !!!\n");   
                 scope++;
                 sleep(3000);}
                if(l == 40){
                  i=0;   
                  for(k=0;k<3;k++)   
                    if(*mano[k] != '0')
                     i++;
                     if(i!=0){
                              printf("\nma questa e' una scopa !!!\n");
                              scope++;
                              sleep(3000);
                              }  
                            } }
                       disegna();
                       q = 0;
                       turno = 1;} 
                         
          else if(sel > card)
          { b = 0;
            strcpy(cartach,"00");
            for(e=0;e<8;e++)
             strcpy(chcard[e],"00");   
            card = 0;  
            arr.x = 2;         
            arr.y = 29;
            arr.g = 25;
            disegna();
            printf("\nle carte selezionate non possono essere prese !!!\n"); }
        }
       }
     }
     

    
void pcmossa(){
     if(turno == 1){
            arr.x = 2;         
            arr.y = 29;
            arr.g = 25;
            card = 0;
            sel = 0;
            somma = 0;
            somma1 = 0;
            apt = 0;
            ch1 = 0;
            strcpy(cartach,"00");
          for(e=0;e<8;e++)
            strcpy(chcard[e],"00");
          for(e=0;e<8;e++)
            strcpy(sommamnt[e],"00");
          
          if(pcheck() != 0){
                      printf("\nil pc ha %d possibilita\n",pcheck());
                      
                      
                   for(e=0;e<3;e++){
                       for(a=0;a<8;a++) {
                                        ch2 = 1;              
                         if(*pchand[e] == *table[a] && *table[a] != '0')
                           {     
                                 if(strcmp(table[a],"7Q") == 0)
                                  ch2 = ch2 + 50;                       
                                 if(strcmp(table[a],"2Q") == 0)
                                  ch2 = ch2 + 10;      
                                 if(strcmp(table[a],"3Q") == 0)
                                  ch2 = ch2 + 10;                    
                                 if(*(table[a] + 1) == 'Q' && *table[a]!= '7' && *table[a] != '2' && *table[a] != '3')
                                  ch2 = ch2 + 1;
                                 if(*table[a] == 7 && *(table[a] + 1) != 'Q')
                                  ch2 = ch2 + 1;
                                  if(strcmp(pchand[e],"7Q") == 0)
                                  ch2 = ch2 + 50;                       
                                 if(strcmp(pchand[e],"2Q") == 0)
                                  ch2 = ch2 + 10;      
                                 if(strcmp(pchand[e],"3Q") == 0)
                                  ch2 = ch2 + 10;                    
                                 if(*(pchand[e] + 1) == 'Q' && *pchand[e]!= '7' && *pchand[e] != '2' && *pchand[e] != '3')
                                  ch2 = ch2 + 1;
                                 if(*pchand[e] == 7 && *(pchand[e] + 1) != 'Q')
                                  ch2 = ch2 + 1;       
                                 
                                 for(n=0;n<8;n++){
                                           if(strcmp(table[n],table[a]) != 0 && *table[n] != '0')
                                            break; }
                                   if(n==8)         
                                     ch2 = ch2 + 100;              
                                 
                                 
                                 if(ch2 > ch1)
                                  {
                                   for(z=0;z<8;z++)
                                   strcpy(chcard[z],"00");
                                   strcpy(cartach,pchand[e]);
                                   strcpy(chcard[0],table[a]);
                                   ch1 = ch2;
                                   //printf("\npotrei prendere %s con %s\n",chcard[0],cartach);
                                   } 
                                    } }
                      ch2 = 1;
                   
                  if(*pchand[e] != '0'){
                       if(apt > 0 && *pchand[e] == 'A'){
                                          
                             for(a=0;a<8;a++){
                                 if(strcmp(table[a],"7Q") == 0)
                                  ch2 = ch2 + 50;                       
                                 if(strcmp(table[a],"2Q") == 0)
                                  ch2 = ch2 + 10;      
                                 if(strcmp(table[a],"3Q") == 0)
                                  ch2 = ch2 + 10;                    
                                 if(*(table[a] + 1) == 'Q' && *table[a]!= '7' && *table[a] != '2' && *table[a] != '3')
                                  ch2 = ch2 + 1;
                                 if(*table[a] == 7 && *(table[a] + 1) != 'Q')
                                  ch2 = ch2 + 1;
                                  }
                            if(ch2 > ch1){
                             ch1 = ch2;
                             for(k=0;k<8;k++)
                              strcpy(chcard[k],"00");
                             
                             for(z=0;z<8;z++)
                               strcpy(chcard[z],table[z]);   
                               strcpy(cartach,pchand[e]);
                              // printf("\noppure pare sia meglio prendere tutto con l'asso\n");
                             } }  }
                       ch2 = 1;      
                ////////////////////////////////////////////                
 for(m=0;m<8;m++){
    if(strcmp(table[m],"00") != 0){               
     somma = ctoi(*table[m]);
     somma1 = ctoi(*table[m]); 
    for(z=0;z<8;z++)
     strcpy(sommamnt[z],"00");
    z=1; 
     strcpy(sommamnt[0],table[m]);
     ch2 = 1;
     for(a=0;a < 8;a++){
         //somma semplice 
           for(l=0;l<8;l++)
            if(*pchand[e] == *table[l] && *pchand[e] != '0')
             break;
        if(l == 8){ 
         
          if( a > m && strcmp(table[a],"00") != 0){  
            somma = somma + ctoi(*table[a]);
             if(*pchand[e] != '0' && ctoi(*pchand[e]) == somma)
                 { ch2 = 0; 
                   if(strcmp(table[m],"7Q") == 0 || strcmp(table[a],"7Q") == 0)
                      ch2 = ch2 + 50; 
                   if(strcmp(table[m],"2Q") == 0 || strcmp(table[a],"2Q") == 0)
                      ch2 = ch2 + 10;
                   if(strcmp(table[m],"3Q") == 0 || strcmp(table[a],"3Q") == 0)
                      ch2 = ch2 + 10;
                   if(strcmp(table[m],"AQ") == 0 || strcmp(table[a],"AQ") == 0)
                      ch2 = ch2 + 10;
                   if(*(table[m] + 1) == 'Q' && *table[m] != '2' && *table[m] != '3' && *table[m] != 'A')
                      ch2 = ch2 + 1;
                   if(*(table[a] + 1) == 'Q' && *table[a] != '2' && *table[a] != '3' && *table[a] != 'A')
                      ch2 = ch2 + 1;
                   if(*table[m] == '7' && *(table[m]+1) != 'Q')
                      ch2 = ch2 + 1;
                   if(*table[a] == '7' && *(table[a]+1) != 'Q')
                      ch2 = ch2 + 1;
                   
                   for(n=0;n<8;n++){
                                    if(strcmp(table[n],table[m]) != 0 && strcmp(table[n],table[a]) != 0 && *table[n] != '0')
                                    break; }
                                   if(n==8)         
                                     ch2 = ch2 + 100;  
                   
                   
                   if(ch2 > ch1 && somma == ctoi(*pchand[e])){
                     ch1 = ch2;
                    for(k=0;k<8;k++)
                     strcpy(chcard[k],"00"); 
                     
                     for(b=0;b<8;b++)
                      strcpy(chcard[b],"00");
                      strcpy(chcard[0],table[m]);
                      strcpy(chcard[1],table[a]);
                      strcpy(cartach,pchand[e]);
                     // printf("\nancora meglio sembra la somma %s + %s con il %s\n",chcard[0],chcard[1],cartach);
                      }    }
                      
                 }
                 
                 
             somma = somma - ctoi(*table[a]);
              
                  //dove si fermava il for con la a     }
          //somma semplice
            
          if( a != m && *table[a] != '0'){              
            if(somma1 + ctoi(*table[a]) < 11){
             // printf("\ncazzo la carta numero %d e' un %c\n",a+1,*table[a]);        
             // printf("\nvisto che %d + %c < 11 somma1 diventa %d\n",somma1,*table[a],somma1 + ctoi(*table[a]));        
              //sleep(30000);
              somma1 = somma1 + ctoi(*table[a]);
              strcpy(sommamnt[z++],table[a]);
              
             for(n=0;n<8;n++)
              if(*sommamnt[n] != '0')
               printf(" %s ",sommamnt[n]);
             
               if(ctoi(*pchand[e]) != '0' && somma1 == ctoi(*pchand[e]) && somma1 - ctoi(*table[a]) != somma )
                 {     ch2 = 1;
                         for(n=0;n<8;n++){
                                          if(strcmp(sommamnt[n],"7Q") == 0)
                                          ch2 = ch2 + 50;
                                          if(strcmp(sommamnt[n],"2Q") == 0)
                                          ch2 = ch2 + 10;
                                          if(strcmp(sommamnt[n],"3Q") == 0)
                                          ch2 = ch2 + 10; 
                                          if(strcmp(sommamnt[n],"AQ") == 0)
                                          ch2 = ch2 + 10;
                                          if(*sommamnt[n] == '7' && *(sommamnt[n]+1) != 'Q')
                                          ch2 = ch2 + 1;
                                          if(*sommamnt[n] != '2' && *sommamnt[n] != 'A' && *sommamnt[n] != '3' && *(sommamnt[n]+1) == 'Q')
                                          ch2 = ch2 + 1;
                                          }
                                          if(strcmp(pchand[e],"7Q") == 0)
                                          ch2 = ch2 + 50;
                                          if(strcmp(pchand[e],"2Q") == 0)
                                          ch2 = ch2 + 10;
                                          if(strcmp(pchand[e],"3Q") == 0)
                                          ch2 = ch2 + 10; 
                                          if(strcmp(pchand[e],"AQ") == 0)
                                          ch2 = ch2 + 10;
                                          if(*pchand[e] == '7' && *(pchand[e]+1) != 'Q')
                                          ch2 = ch2 + 1;
                                          if(*pchand[e] != '2' && *pchand[e] != 'A' && *pchand[e] != '3' && *(pchand[e]+1) == 'Q')
                                          ch2 = ch2 + 1;               
//printf("\nsomma1 :heila pare ci sia una somma (%d + %c) che fa %d con %c\n",somma1-ctoi(*table[a]),*table[a],somma1,*mano[b]); 
              
              for(i=0;i<8;i++){
                       for(k=0;k<8;k++)
                         if(strcmp(sommamnt[k],table[i]) != 0 && *table[i] != '0')
                          i = 10; }       
              if(i == 8)
               ch2 = ch2 +100;
              
              
              
              
               if(ch2 > ch1 && *pchand[e] != '0'){
                       for(k=0;k<8;k++)
                         strcpy(chcard[k],"00");
                         for(n=0;n<8;n++) 
                          strcpy(chcard[n],sommamnt[n]);
                          
                        strcpy(cartach,pchand[e]);
                        //printf("\n ma pare perfetto prendere ");
                        for(n=0;n<8;n++)
                         if(*chcard[n] != '0')
                          printf(" %s",chcard[n]);
                        // printf(" con il %s\n",cartach); 
                        ch1 = ch2;  
                        }
                } 
               }
            // else printf("\ninvece visto che %d + %c > 10 somma1 resta %d\n",somma1,*table[a],somma1);  
              }//blocco somme complessa 
              }// l
              } //parentesi aggiunta
              
       } } }  
                     
                   if(*cartach != 'A')
                    for(e=0;e<8;e++)
                     if(*chcard[e] == *cartach)
                      {for(n=0;n<8;n++)
                        if(*chcard[n] != *cartach)
                         strcpy(chcard[n],"00");
                         break;}
                   
                   //ok ora esegui la mossa  
                   //sleep(3000);
                   disegna();
                   
                 for(k=0;k<8;k++)
                    if(*chcard[k] != '0')
                     break;
                   
                // printf("\n%d, %s\n",k,chcard[k]);
                 
                 
                   printf("\ndunque...\n");
                   //printf("\nch2 e' uguale a %d\nch1 invece %d\n",ch2,ch1);
                   printf("\nora il pc prende il"); 
                   
                  if(k!=8){ 
                   for(e=0;e<8;e++)
                    if(*chcard[e] != '0')
                     printf(" %c%c ",*chcard[e],seme(*(chcard[e]+1)));}
                   
                  else if(k==8)
                    printf("...haha no non prende un cazzo ma se la coniglia");  
                       
                  printf(" con il suo %c%c\n",*cartach,seme(*(cartach + 1)));
                   sleep(3000);
                  
                   for(n=0;n<8;n++){
                        for(z=0;z<8;z++){
                                          if(strcmp(chcard[z],table[n])== 0)
                                           strcpy(table[n],"00");}}
                                           
                   for(n=0;n<3;n++)
                     if(strcmp(cartach,pchand[n]) == 0)
                       strcpy(pchand[n],"00");
                      
                   ///QUA   
            for(e=0;e<40;e++) 
              for(i=0;i<8;i++) 
                if(*puntipc[e] == '0' && *chcard[i] != '0') 
                 { strcpy(puntipc[e],chcard[i]); 
                   strcpy(chcard[i],"00"); } 
             
            for(e=0;e<40;e++)
             if(*puntipc[e] == '0')
              { strcpy(puntipc[e],cartach);
                break; }        
                      
                                                                        
                    
                    
                    disegna();
                    q = 1;
                    turno = 0; 
                    
              for(n=0;n<8;n++)
               if(*table[n] != '0')
                break;
              for(l=0;l<40;l++)
               if(*mazzo[l] != '0')
                break;
              
              if(n==8 && *cartach != 'A'){
                if(l != 40){
                 printf("\nwooo questa e' una scopa !!!\n");   
                 pcscope++;
                 sleep(3000);}
                if(l == 40){
                  i=0;   
                  for(k=0;k<3;k++)   
                    if(*pchand[k] != '0')
                     i++;
                     if(i!=0){
                              printf("\noh yes una scopa !!!\n");
                              pcscope++;
                              sleep(3000);
                              }  
                            }   }  
             /*   
              if(n==8 && *cartach != 'A'){
                 printf("\nquesta e' una scopa !!!\n");   
                 pcscope++;
                 sleep(1500);}
               */     
                   strcpy(cartach,"00");
                    }//if pcheck 
                      
          else {
              // printf("\nil pc ha 0 possibilita\n");
               
              /* 
               for(e=0;e<8;e++)
                if(*table[e] == '0')
                 { for(n=0;n<3;n++)
                    if(*pchand[n] != '0')
                     { strcpy(table[e],pchand[n]);
                       strcpy(cartach,pchand[n]);
                       strcpy(pchand[n],"00");
                       disegna();
                       printf("\nil pc butta il %s\n",cartach);
                       sleep(1000);
                       turno = 0;
                       break;}
                       break;}
               */
// il codice scritto da qui fa scegliere al computer che carta gettare, � una soluzione non raffinatissima ma accettabile..
      //s� pu� rendere il pc ancora pi� intelligente se gli si concede di contare le carte uscite, io non c'ho voglia di farlo
               
               ch1 = 100;
               
             for(e=0;e<3;e++)  
               if(*pchand[e] != '0')
               strcpy(cartach,pchand[e]);
               
               for(e=0;e<3;e++){
                       ch2 = 0;
                    if(*pchand[e] == '2' || *pchand[e] == '3')
                     ch2 = ch2 + 2;
                    if(*(pchand[e]+1) == 'Q')
                     ch2++;            
                    if(*pchand[e] == '7')
                     ch2++;
                    if(strcmp(pchand[e],"7Q") == 0)
                     ch2 = ch2 + 5;             
                         
                   for(m=0;m<8;m++){
                        somma = 0;            
                        if(*table[m] != '0')
                        {            
                           if(ctoi(*table[m]) + ctoi(*pchand[e]) == 3)
                            ch2 = ch2 + 4; 
                           if(ctoi(*table[m]) + ctoi(*pchand[e]) == 7)
                            ch2 = ch2 + 6;       
                                  
                            for(z=0;z<8;z++){
                                if(z != m)
                                 if(somma + ctoi(*table[z]) < 8)
                                    somma = somma + ctoi(*table[z]);          
                                  if(somma == 7)
                                   ch2 = ch2 + 6;           
                                             }//for(z)
                                   
                                   }//if *table[m] != 0 
                                   }//for m               
                               
                              if(ch2 < ch1 && strcmp(pchand[e],"00") != 0){
                                    // printf("\nqui ci sono, ch2 vale %d ch1 %d\n",ch2,ch1);
                                     ch1 = ch2;
                                     //sleep(2000);
                                     strcpy(cartach,pchand[e]);
                                      } 
                                
                                }//for(e)

                                     printf("\nil pc butta il %c%c\n",*cartach,seme(*(cartach+1)));  
                                      sleep(2000); 
                                       
                                       for(z=0;z<8;z++)
                                        if(*table[z] == '0'){
                                         strcpy(table[z],cartach);
                                         break;}
                                        
                                       for(e=0;e<3;e++)
                                        if(strcmp(pchand[e],cartach) == 0)
                                         {  strcpy(pchand[e],"00");
                                             turno = 0;
                                             disegna();
                                             
                                             } 
               
               
               
               }//else   
               }//turno 
              }//pcmossa

char seme(char sm){
     switch(sm){
                case 'C' : return 3;
                case 'Q' : return 4;
                case 'F' : return 5;
                case 'P' : return 6; } }
     
     

void disegna(){
     system("cls");
     distav(); 
     printf("Carte in Tavola :          Le tue Carte :\n");
        
     for(e=0;e<8;e++){
      if(strcmp(table[e],"00") != 0){   
         if(e==0){                      
          tav[1][3] = *table[e];
          tav[1][4] = seme(*(table[e] + 1));
          tav[3][2] = *table[e];
          tav[3][3] = seme(*(table[e] + 1)); }
         else if(e==1){                      
          tav[1][9] = *table[e];
          tav[1][10] = seme(*(table[e] + 1));
          tav[3][8] = *table[e];
          tav[3][9] = seme(*(table[e] + 1)); } 
        else if(e==2){                      
          tav[1][15] = *table[e];
          tav[1][16] = seme(*(table[e] + 1));
          tav[3][14] = *table[e];
          tav[3][15] = seme(*(table[e] + 1)); } 
        else if(e==3){                      
          tav[1][21] = *table[e];
          tav[1][22] = seme(*(table[e] + 1));
          tav[3][20] = *table[e];
          tav[3][21] = seme(*(table[e] + 1)); } 
        else if(e==4){                      
          tav[9][3] = *table[e];
          tav[9][4] = seme(*(table[e] + 1));
          tav[11][2] = *table[e];
          tav[11][3] = seme(*(table[e] + 1)); } 
        else if(e==5){                      
          tav[9][9] = *table[e];
          tav[9][10] = seme(*(table[e] + 1));
          tav[11][8] = *table[e];
          tav[11][9] = seme(*(table[e] + 1)); } 
        else if(e==6){                      
          tav[9][15] = *table[e];
          tav[9][16] = seme(*(table[e] + 1));
          tav[11][14] = *table[e];
          tav[11][15] = seme(*(table[e] + 1)); }
        else if(e==7){                      
          tav[9][21] = *table[e];
          tav[9][22] = seme(*(table[e] + 1));
          tav[11][20] = *table[e];
          tav[11][21] = seme(*(table[e] + 1)); } 
          }
          }   
    
    for(e=0;e<3;e++){
            if(strcmp(mano[e],"00") != 0)
             {  if(e==0){
                         tav[5][29] = *mano[e];          
                         tav[5][30] = seme(*(mano[e]+1));  
                         tav[7][28] = *mano[e];
                         tav[7][29] = seme(*(mano[e]+1));}
                if(e==1){
                         tav[5][35] = *mano[e];          
                         tav[5][36] = seme(*(mano[e]+1));  
                         tav[7][34] = *mano[e];
                         tav[7][35] = seme(*(mano[e]+1));}
                if(e==2){
                         tav[5][41] = *mano[e];          
                         tav[5][42] = seme(*(mano[e]+1));  
                         tav[7][40] = *mano[e];
                         tav[7][41] = seme(*(mano[e]+1));}                  
                          }     }   
                         
          for(e=0;e<11;e++)
            {  
              if(arr.x != 0)
              tav[arr.x][arr.y] = arr.g;
               }              
                         
    
          
       
       for(e=0;e<13;e++){
                   printf("\n");
  for(i=0;i<50;i++){
    if(tav[e][i] == 3 || tav[e][i]== 4){
                      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),508);
                      printf("%c",tav[e][i]);}
    else{                  
      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),496);
      printf("%c",tav[e][i]);}}}
  if(strcmp(usr,"mtn++") == 0)  
     printf("\nmano del pc : %s,%s,%s\n",pchand[0],pchand[1],pchand[2]);
  if(strcmp(usr,"mtn++") != 0)  
     printf("\nmano del pc : eh,eh,eh\n",pchand[0],pchand[1],pchand[2]);      
        }
     
int ctoi(char cd){
   switch (cd) {
           case 'A' : return 1; case '6' : return 6;    
           case '2' : return 2; case '7' : return 7;
           case '3' : return 3; case 'J' : return 8;
           case '4' : return 4; case 'Q' : return 9;
           case '5' : return 5; case 'K' : return 10; }
           }
           


void contapunti(){
     system("cls");
     printf("hai totalizzato questi punti :\n"); 
    for(e=0;e<40;e++){
                 if(strcmp(punti[e],"7Q") == 0)
                  { printf("\nsettebello\n");
                    points++;}
                 if(*(punti[e]+1) == 'Q')
                  quadri++;      
                 if(*punti[e] != '0')
                  carte++;     
                      } 
      
      if(quadri > 5){
      printf("\nhai %d/10 carte di denari\n",quadri);
      points++;}
      
      if(quadri == 10){
       printf("\naspetta ma questo si chiama napoleone a casa mia, hai stravinto !!\n");
       points = 1000;}
      
      if(carte > 20){
      printf("\nhai fatto carte\n");
      points++;}
      
      for(e=0;e<4;e++)
               for(i=0;i<40;i++)
                 if(*punti[i] != '0' && prim(*punti[i]) > prim(*primiera[e]))
                   { if(e == 0 && *(punti[i] +1) == 'C')
                      strcpy(primiera[e],punti[i]);        
                     if(e == 1 && *(punti[i] +1) == 'Q')
                      strcpy(primiera[e],punti[i]);
                      if(e == 2 && *(punti[i] +1) == 'F')
                      strcpy(primiera[e],punti[i]);
                      if(e == 3 && *(punti[i] +1) == 'P')
                      strcpy(primiera[e],punti[i]); }
      for(e=0;e<4;e++)
       primi = primi + prim(*primiera[e]);
      
     if(strcmp(usr,"mtn++")==0){ 
      printf("\ntua primiera : ");
      
       for(e=0;e<4;e++)
        printf(" %s ",primiera[e]);}
      
      for(e=0;e<4;e++)
       strcpy(primiera[e],"00");
      
      
      for(e=0;e<4;e++)
               for(i=0;i<40;i++)
                 if(*puntipc[i] != '0' && prim(*puntipc[i]) > prim(*primiera[e]))
                   { if(e == 0 && *(puntipc[i] +1) == 'C')
                      strcpy(primiera[e],puntipc[i]);        
                     if(e == 1 && *(puntipc[i] +1) == 'Q')
                      strcpy(primiera[e],puntipc[i]);
                     if(e == 2 && *(puntipc[i] +1) == 'F')
                      strcpy(primiera[e],puntipc[i]);
                     if(e == 3 && *(puntipc[i] +1) == 'P')
                      strcpy(primiera[e],puntipc[i]); }       
      
      for(e=0;e<4;e++)
       pcprimiera = pcprimiera + prim(*primiera[e]);
      
      
      if(primi > pcprimiera){
               printf("\nprimiera ");
               points++;}
      
      printf("\nhai fatto %d scope\n",scope);     
      
      for(e=0;e<40;e++){
        if(strcmp(punti[e],"AQ") == 0)
        napula[0] = 1;
        if(strcmp(punti[e],"2Q") == 0)
        napula[1] = 1;
        if(strcmp(punti[e],"3Q") == 0)
        napula[2] = 1;
        if(strcmp(punti[e],"4Q") == 0)
        napula[3] = 1;
        if(strcmp(punti[e],"5Q") == 0)
        napula[4] = 1;
        if(strcmp(punti[e],"6Q") == 0)
        napula[5] = 1;
        if(strcmp(punti[e],"7Q") == 0)
        napula[6] = 1;
        if(strcmp(punti[e],"JQ") == 0)
        napula[7] = 1;
        if(strcmp(punti[e],"QQ") == 0)
        napula[8] = 1;
        if(strcmp(punti[e],"KQ") == 0)
        napula[9] = 1; 
         }
     if(napula[0] == 1 && napula[1] == 1 && napula[2] == 1){
                points = points + 3;   
      for(e=3;e<10;e++){
                 if(napula[e] == 0)
                 break;       
                 else
                 points++;       
                        }
                      
             printf("\ncazzo hai fatto un po' di punti di napola, complimenti..!\n");        
                        }
      
      
      
      for(e=0;e<10;e++)
      napula[e] = 0;
      
      
      points = points + scope;
      
               
     printf("\nmentre il pc ha fatto : \n");
       for(e=0;e<40;e++){
                 if(strcmp(puntipc[e],"7Q") == 0)
                  { printf("\nsettebello\n");
                    pcpoints++;}}
     
     if(quadri < 5){
               printf("\n%d/10 carte di denari\n",10-quadri);
               pcpoints++;}
      
     if(quadri == 0){
               printf("\nmi dispiace fai davvero schifo, il pc ti ha spezzato. Napoleone.\n");
               pcpoints = 1000;}
               
               
     if(carte < 20){          
               printf("\npiu' carte, in totale %d\n",40-carte);
               pcpoints++;} 
     
     if(strcmp(usr,"mtn++")==0){ 
      printf("\npc primiera : ");
      
       for(e=0;e<4;e++)
        printf(" %s ",primiera[e]);}
     
     
     if(pcprimiera > primi){
                   printf("\nprimiera\n");
                   pcpoints++;}
     
     for(e=0;e<40;e++){
        if(strcmp(puntipc[e],"AQ") == 0)
        napula[0] = 1;
        if(strcmp(puntipc[e],"2Q") == 0)
        napula[1] = 1;
        if(strcmp(puntipc[e],"3Q") == 0)
        napula[2] = 1;
        if(strcmp(puntipc[e],"4Q") == 0)
        napula[3] = 1;
        if(strcmp(puntipc[e],"5Q") == 0)
        napula[4] = 1;
        if(strcmp(puntipc[e],"6Q") == 0)
        napula[5] = 1;
        if(strcmp(puntipc[e],"7Q") == 0)
        napula[6] = 1;
        if(strcmp(puntipc[e],"JQ") == 0)
        napula[7] = 1;
        if(strcmp(puntipc[e],"QQ") == 0)
        napula[8] = 1;
        if(strcmp(puntipc[e],"KQ") == 0)
        napula[9] = 1; 
         }
     if(napula[0] == 1 && napula[1] == 1 && napula[2] == 1){
                pcpoints = pcpoints + 3;   
      for(e=3;e<10;e++){
                 if(napula[e] == 0)
                 break;       
                 else
                 pcpoints++;       
                        }
             printf("\nmmm il pc ha fatto dei di punti di napola, complimenti..!\n");        
                        }
     
     
     printf("\n%d scope\n",pcscope);
    
     pcpoints = pcpoints + pcscope;
     
     if(carte == 20)
      printf("\ntu e il pc avete lo stesso numero di carte\n");
     
     if(quadri == 5)
      printf("\navete lo stesso numero di quadri\n");
     
     if(pcprimiera == primi)
      printf("\nla primiera e' pari\n");
     
     printf("\nin totale avete totalizzato tu : %d e il pc : %d\n",points,pcpoints);
     printf("\n\npremere invio per continuare\n");
     //printf("\nps : non so perch� ma lo prende in ritaro, premere invio un po' di volte");
     
     
     while(1)
      if(GetKeyState(13) < 0)
       break;
     
     }




void reset(){
     
     if(ini == 0)
     { ini = 1;
       turno = 0;}
     else if(ini == 1)
      { ini = 0;
        turno = 1;}  
     
     for(e=0;e<40;e++)
     { strcpy(mazzo[e],easy[e]);
       strcpy(punti[e],"00");
       strcpy(puntipc[e],"00");}
     
     
     for(e=0;e<8;e++)
      strcpy(table[e],"00");
     for(e=0;e<8;e++)
      strcpy(sommamnt[e],"00");
     
      for(i=0;i<4;i++){ 
      e = rand()%100;
     if(e < 40 && strcmp(mazzo[e],"00") != 0){
          strcpy(table[i],mazzo[e]);
          //printf("%s\n",table[i]);
          mazzo[e] = malloc(sizeof(char)*2);
          strcpy(mazzo[e],"00");
          }
     else i--;}                 
   for(i=i;i<8;i++)
    strcpy(table[i],"00");
     
     pesca();
     
     for(e=0;e<4;e++)
      strcpy(primiera[e],"00");
      
     for(e=0;e<10;e++)
      napula[e] = 0; 
      
      quadri = 0;
      scope = 0;
      pcscope = 0;
      
      disegna(); 
       }

int prim(char pm){
     switch(pm){
              case '7' : return 21;   case '6' : return 18;  
              case 'A' : return 16;   case '5' : return 15; 
              case '4' : return 14;   case '2' : return 12; 
              case '3' : return 13;   case 'Q' : return 10;
              case 'J' : return 10;   case 'K' : return 10;
              case '0' : return 0;
              }    
         }








































































































void distav(){
     for(e=0;e<13;e++)
  for(i=0;i<50;i++)
   tav[e][i] = ' ';
   
  for(e=0;e<24;e++){
   if(e != 0 && e!= 1 && e!= 5 && e!= 6 && e!= 7 && e!= 11 && e!= 12 && e!= 13 && e!= 17 && e!= 18 && e!= 19 && e!= 23)
      tav[0][e] = '-';
   if(e != 0 && e!= 1 && e!= 5 && e!= 6 && e!= 7 && e!= 11 && e!= 12 && e!= 13 && e!= 17 && e!= 18 && e!= 19 && e!= 23)
      tav[4][e] = '-';   
   if(e != 0 && e!= 1 && e!= 5 && e!= 6 && e!= 7 && e!= 11 && e!= 12 && e!= 13 && e!= 17 && e!= 18 && e!= 19 && e!= 23)
      tav[8][e] = '-';  
   if(e != 0 && e!= 1 && e!= 5 && e!= 6 && e!= 7 && e!= 11 && e!= 12 && e!= 13 && e!= 17 && e!= 18 && e!= 19 && e!= 23)
      tav[12][e] = '-';  
     }
   
   for(e=26;e<44;e++){
   if(e != 26 && e!= 27 && e!= 31 && e!= 32 && e!= 33 && e!= 37 && e!= 38 && e!= 39 && e!= 43)
      tav[4][e] = '-';
   if(e != 26 && e!= 27 && e!= 31 && e!= 32 && e!= 33 && e!= 37 && e!= 38 && e!= 39 && e!= 43)
      tav[8][e] = '-';    }
   
   for(e=4;e<12;e++){
    if(e != 0 && e != 1 && e != 2 && e != 3 && e != 4 && e != 8 && e != 9 && e != 10 && e != 11 && e != 12)               
       tav[e][27] = '|'; 
    if(e != 0 && e != 1 && e != 2 && e != 3 && e != 4 && e != 8 && e != 9 && e != 10 && e != 11 && e != 12)               
       tav[e][31] = '|';   
    if(e != 0 && e != 1 && e != 2 && e != 3 && e != 4 && e != 8 && e != 9 && e != 10 && e != 11 && e != 12)               
       tav[e][33] = '|';
    if(e != 0 && e != 1 && e != 2 && e != 3 && e != 4 && e != 8 && e != 9 && e != 10 && e != 11 && e != 12)               
       tav[e][37] = '|';   
    if(e != 0 && e != 1 && e != 2 && e != 3 && e != 4 && e != 8 && e != 9 && e != 10 && e != 11 && e != 12)               
       tav[e][39] = '|';   
    if(e != 0 && e != 1 && e != 2 && e != 3 && e != 4 && e != 8 && e != 9 && e != 10 && e != 11 && e != 12)               
       tav[e][43] = '|';   
       }
   
     
   for(e=0;e<13;e++){
    if(e != 0 && e != 4 && e != 5 && e != 6 && e !=  7 && e != 8 && e != 12)                  
      tav[e][1] = '|';
    if(e != 0 && e != 4 && e != 5 && e != 6 && e !=  7 && e != 8 && e != 12)                  
      tav[e][5] = '|';  
    if(e != 0 && e != 4 && e != 5 && e != 6 && e !=  7 && e != 8 && e != 12)                  
      tav[e][7] = '|';  
    if(e != 0 && e != 4 && e != 5 && e != 6 && e !=  7 && e != 8 && e != 12)                  
      tav[e][11] = '|';
    if(e != 0 && e != 4 && e != 5 && e != 6 && e !=  7 && e != 8 && e != 12)                  
      tav[e][13] = '|';  
    if(e != 0 && e != 4 && e != 5 && e != 6 && e !=  7 && e != 8 && e != 12)                  
      tav[e][17] = '|';  
    if(e != 0 && e != 4 && e != 5 && e != 6 && e !=  7 && e != 8 && e != 12)                  
      tav[e][19] = '|';  
    if(e != 0 && e != 4 && e != 5 && e != 6 && e !=  7 && e != 8 && e != 12)                  
      tav[e][23] = '|'; 
      } }
